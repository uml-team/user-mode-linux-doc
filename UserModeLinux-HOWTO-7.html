<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
 <META NAME="GENERATOR" CONTENT="LinuxDoc-Tools 0.9.21">
 <TITLE>User Mode Linux HOWTO : Sharing Filesystems between Virtual Machines</TITLE>
 <LINK HREF="UserModeLinux-HOWTO-8.html" REL=next>
 <LINK HREF="UserModeLinux-HOWTO-6.html" REL=previous>
 <LINK HREF="UserModeLinux-HOWTO.html#toc7" REL=contents>
</HEAD>
<BODY>
<A HREF="UserModeLinux-HOWTO-8.html">Next</A>
<A HREF="UserModeLinux-HOWTO-6.html">Previous</A>
<A HREF="UserModeLinux-HOWTO.html#toc7">Contents</A>
<HR>
<H2><A NAME="shared_fs"></A> <A NAME="s7">7.</A> <A HREF="UserModeLinux-HOWTO.html#toc7">Sharing Filesystems between Virtual Machines</A></H2>

<P> </P>

<H2><A NAME="ss7.1">7.1</A> <A HREF="UserModeLinux-HOWTO.html#toc7.1">A warning</A>
</H2>

<P>Don't attempt to share filesystems simply by booting two UMLs from the
same file.  That's the same thing as booting two physical machines
from a shared disk.  It will result in filesystem corruption.</P>


<H2><A NAME="ss7.2">7.2</A> <A HREF="UserModeLinux-HOWTO.html#toc7.2">Using layered block devices</A>
</H2>

<P>The way to share a filesystem between two virtual machines is to use
the copy-on-write (COW) layering capability of the ubd block driver.  As of
2.4.6-2um, the driver supports layering a read-write private device
over a read-only shared device.  A machine's writes are stored in the
private device, while reads come from either device - the private one
if the requested block is valid in it, the shared one if not.  Using
this scheme, the majority of data which is unchanged is shared between
an arbitrary number of virtual machines, each of which has a much
smaller file containing the changes that it has made.  With a large
number of UMLs booting from a large root filesystem, this leads to a
huge disk space saving.  It will also help performance, since the host
will be able to cache the shared data using a much smaller amount of
memory, so UML disk requests will be served from the host's memory
rather than its disks.  </P>

<P> </P>
<P>To add a copy-on-write layer to an existing block device file, simply
add the name of the COW file to the appropriate ubd switch:
<BLOCKQUOTE><CODE>
<PRE>
 ubd0=root_fs_cow,root_fs_debian_22
</PRE>
</CODE></BLOCKQUOTE>

where 'root_fs_cow' is the private COW file and 'root_fs_debian_22' is
the existing shared filesystem.  The COW file need not exist.  If it
doesn't, the driver will create and initialize it.  Once the COW file
has been initialized, it can be used on its own on the command line:
<BLOCKQUOTE><CODE>
<PRE>
 ubd0=root_fs_cow
</PRE>
</CODE></BLOCKQUOTE>

The name of the backing file is stored in the COW file header, so it
would be redundant to continue specifying it on the command line.</P>


<H2><A NAME="ss7.3">7.3</A> <A HREF="UserModeLinux-HOWTO.html#toc7.3">Note!</A>
</H2>

<P>When checking the size of the COW file in order to see the gobs of
space that you're saving, make sure you use 'ls -ls' to see the actual
disk consumption rather than the length of the file.  The COW file is
sparse, so the length will be very different from the disk usage.
Here is a 'ls -l' of a COW file and backing file from one boot and
shutdown:
<BLOCKQUOTE><CODE>
<PRE>
host% ls -l cow.debian debian2.2
-rw-r--r--    1 jdike    jdike    492504064 Aug  6 21:16 cow.debian
-rwxrw-rw-    1 jdike    jdike    537919488 Aug  6 20:42 debian2.2
</PRE>
</CODE></BLOCKQUOTE>

Doesn't look like much saved space, does it?  Well, here's 'ls -ls':
<BLOCKQUOTE><CODE>
<PRE>
host% ls -ls cow.debian debian2.2
   880 -rw-r--r--    1 jdike    jdike    492504064 Aug  6 21:16 cow.debian
525832 -rwxrw-rw-    1 jdike    jdike    537919488 Aug  6 20:42 debian2.2
</PRE>
</CODE></BLOCKQUOTE>

Now, you can see that the COW file has less than a meg of disk, rather
than 492 meg.</P>


<H2><A NAME="ss7.4">7.4</A> <A HREF="UserModeLinux-HOWTO.html#toc7.4">Another warning</A>
</H2>

<P>Once a filesystem is being used as a readonly backing file for a COW
file, do not boot directly from it or modify it in any way.  Doing so
will invalidate any COW files that are using it.  The mtime and size
of the backing file are stored in the COW file header at its creation,
and they must continue to match.  If they don't, the driver will
refuse to use the COW file.</P>

<P> </P>
<P>If you attempt to evade this restriction by changing either the
backing file or the COW header by hand, you will get a corrupted
filesystem.</P>

<P> </P>
<P>Among other things, this means that upgrading the distribution in a
backing file and expecting that all of the COW files using it will see
the upgrade will not work.  </P>



<H2><A NAME="ss7.5">7.5</A> <A HREF="UserModeLinux-HOWTO.html#toc7.5">Moving a backing file</A>
</H2>

<P>Because UML stores the backing file name and its mtime in the COW
header, if you move the backing file, that information becomes
invalid.  So, the procedure for moving a backing file is
<UL>
<LI>Move it in a way that preserves timestamps.  Usually, this is a &quot;-p&quot;
switch.  &quot;cp -a&quot; works because &quot;-a&quot; implies &quot;-p&quot;.
</LI>
<LI>Update the COW header by booting UML on it, specifying both the COW
file and the new location of the backing file
<BLOCKQUOTE><CODE>
<PRE>
host% ubd0=COW file,new backing file
location
</PRE>
</CODE></BLOCKQUOTE>

UML will notice the mismatch between the command line and COW header,
check the size and mtime of the new backing file path, and update the
COW header to reflect it if it checks out.
</LI>
</UL>

If you forget to preserve the timestamps when you move the backing
file, you can fix the mtime by hand as follows
<BLOCKQUOTE><CODE>
<PRE>
host% 
mtime=whatever UML says mtime should be ; \
touch --date=&quot;`date -d 1970-01-01\ UTC\ $mtime\ seconds`&quot; backing file
</PRE>
</CODE></BLOCKQUOTE>

Note that if you do this on a backing file that has truly been
changed, and not just moved, then you will get file corruption and you
will lose the filesystem.</P>


<H2><A NAME="ss7.6">7.6</A> <A HREF="UserModeLinux-HOWTO.html#toc7.6">uml_moo : Merging a COW file with its backing file</A>
</H2>

<P>Depending on how you use UML and COW devices, it may be advisable to
merge the changes in the COW file into the backing file every once in
a while.</P>

<P> </P>
<P>The utility that does this is uml_moo.  Its usage is
<BLOCKQUOTE><CODE>
<PRE>
host% uml_moo COW file new backing file
</PRE>
</CODE></BLOCKQUOTE>

There's no need to specify the backing file since that information is
already in the COW file header.  If you're paranoid, boot the new
merged file, and if you're happy with it, move it over the old backing
file.</P>

<P> </P>
<P>uml_moo creates a new backing file by default as a safety measure.  It
also has a destructive merge option which will merge the COW file
directly into its current backing file.  This is really only usable
when the backing file only has one COW file associated with it.  If
there are multiple COWs associated with a backing file, a -d merge of
one of them will invalidate all of the others.  However, it is
convenient if you're short of disk space, and it should also be
noticably faster than a non-destructive merge.  This usage is
<BLOCKQUOTE><CODE>
<PRE>
host% uml_moo -d COW file
</PRE>
</CODE></BLOCKQUOTE>
</P>

<P> </P>
<P>uml_moo is installed with the UML deb and RPM.  If you didn't install
UML from one of those packages, you can also get it from the 
<A HREF="http://user-mode-linux.sourceforge.net/dl-sf.html#UML utilities">UML utilities</A>  tar file in tools/moo.</P>


<H2><A NAME="ss7.7">7.7</A> <A HREF="UserModeLinux-HOWTO.html#toc7.7">uml_mkcow : Create a new COW file</A>
</H2>


<P>The normal way to create a COW file is to specify a non-existant COW
file on the UML command line, and let UML create it for you.  However,
sometimes you want a new COW file, and you don't want to boot UML in
order to get it.  This can be done with uml_mkcow, which is a little
standalone utility by Steve Schnepp.</P>
<P> 
The standard usage is 
<BLOCKQUOTE><CODE>
<PRE>
host% uml_mkcow new COW file existing
backing file
</PRE>
</CODE></BLOCKQUOTE>

If you want to destroy an existing COW file, then there is a -f switch
to force the overwriting of the old COW file
<BLOCKQUOTE><CODE>
<PRE>
host% uml_mkcow -f existing COW file existing
backing file
</PRE>
</CODE></BLOCKQUOTE>
</P>
<P> 
uml_mkcow is available from the  
<A HREF="http://user-mode-linux.sourceforge.net/dl-sf.html#UML utilities">UML utilities</A>  tar file in
tools/moo.</P>







<HR>
<A HREF="UserModeLinux-HOWTO-8.html">Next</A>
<A HREF="UserModeLinux-HOWTO-6.html">Previous</A>
<A HREF="UserModeLinux-HOWTO.html#toc7">Contents</A>
</BODY>
</HTML>
